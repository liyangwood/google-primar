import { NgModule } from "@angular/core";

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CommonModule as AngularCommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  providers: [
    
  ],
  imports: [
    NgZorroAntdModule,
    AngularCommonModule,
    FormsModule
  ],
  declarations: [

  ],
  exports: [
    NgZorroAntdModule,
    AngularCommonModule,
    FormsModule
  ]
})
export class CommonModule {

}

