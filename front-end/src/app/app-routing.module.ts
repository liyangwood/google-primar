import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { 
    path: '', 
    pathMatch: 'full', 
    redirectTo: 'lessons' 
  },
  { 
    path: 'lessons', 
    loadChildren: './pages/lessons/lessons.module#LessonsModule'
  },
  {
    path: 'lessons/upload',
    loadChildren: './pages/upload_lesson/upload_lesson.module#UploadLessonModule'
  },
  {
    path: 'mini-course',
    loadChildren: './pages/mini_course/minicourse.module#MiniCourseModule'
  },
  {
    path: 'category-skill',
    loadChildren: './pages/category_skill/category_skill.module#CategoryAndSkillModule'
  },
  {
    path: 'qrcode-generator',
    loadChildren: './pages/qrcode_generator/qrcode_generator.module#QrCodeGeneratorModule'
  },
  {
    path: 'reporting',
    loadChildren: './pages/reporting/reporting.module#ReportingModule'
  },
  {
    path: 'dashboard',
    loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: "reload" })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
