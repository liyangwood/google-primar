import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import {UtilService} from './UtilService'

const key = 'gp-loading';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  constructor(
    private util: UtilService
  ){}

  show(){
    this.util.publish(key, true);
  }
  hide(){
    this.util.publish(key, false);
  }
}