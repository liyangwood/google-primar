import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  axios = null;
  constructor(

  ){
    
    const _axios = axios.create({
      baseURL: '/',
      // baseURL: '/api'
    });

    _axios.interceptors.request.use((req)=>{
      // req.headers.Authorization = this.userData.token;
      return req;
    });

    _axios.interceptors.response.use((res)=>{
      console.log("axios response => ", res);
      if(res.data){
        return Promise.resolve(res.data);
      }
    }, (error)=>{
      return Promise.reject(error);
    });

    this.axios = _axios;
  }

  
  async test(){
    const url = `http://www.baidu.com`;
    return this.axios.get(url);

  }

  async getLessonsCategoryData(category_id?){

    const arr = ['邮件营销', '社交媒体营销', '数字营销基础', '内容营销', '数据分析', '商机洞察', '移动营销', '视频营销', '数字营销进阶', '供应商管理', '销售技巧', '商业管理', '创业公司'];
    const D = _.map(arr, (key)=>{
      const tmp = {
        name : key,
        total : 5,
        list : []
      };
      _.each(_.range(tmp.total), (n)=>{
        const index = n+1;
        tmp.list.push({
          title : key+'-'+index,
          category : '数字营销&效果衡量',
          skill : key,
          date : '2019 Mar 22',
          status : n%2>0 ? 'public' : 'hidden'
        });
      });

      return tmp;
    })

    return new Promise((resolve)=>{

      _.delay(()=>{
        resolve(D);
      }, 500);
    });
  }

  getMinicourseList(){
    const D = [
      {
        title : '打造专业独立站，圈粉海外新市场',
        num : 11,
        date : '2019 Nov 27',
        status : 'public'
      },
      {
        title : '巧用谷歌分析，数字营销效果翻倍',
        num : 3,
        date : '2019 Nov 27',
        status : 'public'
      },
      {
        title : '数字营销入门：如何低成本实现高效营销',
        num : 12,
        date : '2019 Nov 27',
        status : 'public'
      },
      {
        title : '创业走好这几步，梦想“变现”并不难',
        num : 1,
        date : '2019 Nov 27',
        status : 'public'
      },
      {
        title : '移动应用如何圈粉海外，成功变现',
        num : 121,
        date : '2019 Nov 27',
        status : 'public'
      }
    ];

    return new Promise((resolve)=>{

      _.delay(()=>{
        resolve(D);
      }, 500);
    });
  }
};