import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import PubSub from 'pubsub-js';


@Injectable({
  providedIn: 'root'
})
export class UtilService {
  public register = PubSub.subscribe;
  public publish = PubSub.publish;
  constructor(){}

}