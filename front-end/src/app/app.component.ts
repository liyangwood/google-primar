import { Component } from '@angular/core';
import {UtilService} from './service/UtilService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isCollapsed = false;
  loading = false;

  constructor(
    public util: UtilService
  ){
    this.initEvent();
  }

  initEvent(){
    this.util.register('gp-loading', (key, f)=>{
      this.loading = f ? true : false;
    })
  }

  clickLogout(){
    alert(1);

  }
}
