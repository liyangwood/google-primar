import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrCodeGeneratorComponent } from './qrcode_generator.component';

const routes: Routes = [
  { path: '', component: QrCodeGeneratorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrCodeGeneratorRoutingModule { }
