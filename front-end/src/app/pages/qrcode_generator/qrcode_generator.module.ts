import { NgModule } from '@angular/core';

import { QrCodeGeneratorRoutingModule } from './qrcode_generator-routing.module';

import { QrCodeGeneratorComponent } from './qrcode_generator.component';


@NgModule({
  imports: [QrCodeGeneratorRoutingModule],

  declarations: [QrCodeGeneratorComponent]
})
export class QrCodeGeneratorModule { }
