import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryAndSkillComponent } from './category_skill.component';

const routes: Routes = [
  { path: '', component: CategoryAndSkillComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryAndSkillRoutingModule { }
