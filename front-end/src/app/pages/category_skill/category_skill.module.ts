import { NgModule } from '@angular/core';

import { CategoryAndSkillRoutingModule } from './category_skill-routing.module';

import { CategoryAndSkillComponent } from './category_skill.component';


@NgModule({
  imports: [CategoryAndSkillRoutingModule],
  declarations: [CategoryAndSkillComponent],
})
export class CategoryAndSkillModule { }
