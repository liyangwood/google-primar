import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadLessonComponent } from './upload_lesson.component';
import {CommonModule} from '../../common/common.module';

const routes: Routes = [
  { 
    path: '', 
    component: UploadLessonComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  declarations: [UploadLessonComponent]
})
export class UploadLessonModule { }
