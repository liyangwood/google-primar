import { Component, OnInit, Input, NgModule } from '@angular/core';
import {CommonModule} from '../../common/common.module';
import { BrowserModule } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
  selector: 'gp-lessons-category-table',
  templateUrl: './CategoryTable.html',
})
class LessonsCategoryTableComponent {
  @Input() table_data: any[];
  @Input() title: String;

  constructor(
    
  ) {}


  clickSwitch(d){
    console.log(d);
  }

}

@NgModule({
  imports: [
    CommonModule
  ],

  declarations: [LessonsCategoryTableComponent],
  exports: [LessonsCategoryTableComponent]
})
export class LessonsCategoryTableModule { }
