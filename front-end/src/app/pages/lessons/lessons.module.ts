import { NgModule } from '@angular/core';

import { LessonsRoutingModule } from './lessons-routing.module';

import { LessonsComponent } from './lessons.component';
import { LessonsCategoryTableModule } from './CategoryTable';
import {CommonModule} from '../../common/common.module';

@NgModule({
  imports: [
    CommonModule,
    LessonsRoutingModule,
    LessonsCategoryTableModule,
    
  ],

  declarations: [LessonsComponent]
})
export class LessonsModule { }
