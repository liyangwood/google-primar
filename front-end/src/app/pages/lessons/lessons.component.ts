import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../service/HttpService';
import {LoadingService} from '../../service/LoadingService';
import { Router } from "@angular/router";
import * as _ from 'lodash';

const C = {
  category : [
    // {
    //   id : 0,
    //   name : 'All'
    // },
    {
      id : 1,
      name : '数字营销&效果衡量'
    },
    {
      id : 2,
      name : '商业策略&操作技巧'
    },
    {
      id : 3,
      name : '品牌打造&产品设计'
    }
  ]
};

@Component({
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit {

  category_data = C.category;
  data:any = null;

  constructor(
    private http: HttpService,
    private loading: LoadingService,
    private router: Router,
  ) { }

  async ngOnInit() {
    this.loading.show();
    const d = await this.http.getLessonsCategoryData();
    this.data = this.processTableData(d);
    this.loading.hide();
  }

  processTableData(d){
    return _.map(d, (x)=>{
      x.list = _.map(x.list, (item)=>{
        item.public = item.status === 'public';

        return item;
      });
      return x;
    });
  }

  goToUploadPage(){
    this.router.navigateByUrl('/lessons/upload');
  }

}
