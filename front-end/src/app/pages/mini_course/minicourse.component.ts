import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../service/HttpService';
import {LoadingService} from '../../service/LoadingService';
import { Router } from "@angular/router";
import * as _ from 'lodash';

@Component({
  templateUrl: './minicourse.component.html',
  styleUrls: ['./minicourse.component.scss']
})
export class MiniCourseComponent implements OnInit {

  table_data = null;


  constructor(
    private http: HttpService,
    private loading: LoadingService,
    private router: Router,
  ){ 

  }

  async ngOnInit() {
    this.loading.show();
    const list = await this.http.getMinicourseList();
    this.table_data = this.processTableData(list);

    this.loading.hide();
  }

  processTableData(d){
    return _.map(d, (x)=>{
      x.list = _.map(x.list, (item)=>{
        item.public = item.status === 'public';
        d.loading = false;
        return item;
      });
      return x;
    });
  }


  goToCreatePage(){

  }

  clickSwitch(d){
    d.loading = true;
  }
}
