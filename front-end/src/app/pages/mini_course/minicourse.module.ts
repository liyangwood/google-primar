import { NgModule } from '@angular/core';

import { MiniCourseRoutingModule } from './minicourse-routing.module';

import { MiniCourseComponent } from './minicourse.component';
import {CommonModule} from '../../common/common.module';


@NgModule({
  imports: [
    CommonModule,
    MiniCourseRoutingModule
  ],
  declarations: [MiniCourseComponent],

})
export class MiniCourseModule { }
