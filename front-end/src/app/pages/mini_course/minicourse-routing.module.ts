import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiniCourseComponent } from './minicourse.component';

const routes: Routes = [
  { path: '', component: MiniCourseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiniCourseRoutingModule { }
