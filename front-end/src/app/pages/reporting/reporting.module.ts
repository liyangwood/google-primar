import { NgModule } from '@angular/core';

import { ReportingRoutingModule } from './reporting-routing.module';

import { ReportingComponent } from './reporting.component';


@NgModule({
  imports: [ReportingRoutingModule],

  declarations: [ReportingComponent]
})
export class ReportingModule { }
